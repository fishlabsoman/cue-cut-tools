#!/usr/bin/php
<?php

/*
README:
1. need installed media-libs/libid3tag (ex v0.15.1b-r4)
2. need media-video/mediainfo (ex v0.7.65)
3. need media-video/ffmpeg (ex v1.2.6-r1)
typical using cue.php filename.flac [filename.flac]
*/


//$pwd = $_SERVER["PWD"];

unset($_SERVER);
unset($_POST);
unset($_COOKIE);
unset($_FILES);
unset($_GET);

$threads    = 24;//nember of threads
$uniqId        = rand(100, 999).rand(100, 999).rand(100, 999).rand(100, 999); //id for grep "ps ax"

echo "My process uniqId: $uniqId\r\n";


if(!isset($argv[1]) || strlen($argv[1]) < 1) {
    die("\r\nNo input files\n");
}

$cue             = "";
$musicFile        = "";
$musicFileExt    = "";
$musicExt         = ["flac","ape", "mp3", "aac", "m4a", "wav", "wv"]; //TODO - need encoding other formats to flac

$dir  = str_replace(basename($argv[1]), "", $argv[1]);//remove basename from path


//parse argv
for($i = 1;$i < count($argv);$i++){
    $file = $argv[$i];
    $file = explode('.', $file);//split for finding extention
    $extention = $file[count($file) - 1];
    
    if($extention == 'cue'){
        $cue = $argv[$i];
        echo "cue: \"{$cue}\"\n";
        //logic for abc.flac.cue -> abc.flac
        $file = explode('.', $argv[$i]);//split for finding extention
        unset($file[count($file)-1]);//remove ".cue"
        
        for($j = 0;$j < count($musicExt);$j++){
            
            if($file[count($file)-1] == $musicExt[$j]){
                
                if(file_exists (implode(".",$file))){
                    
                    $musicFileExt    = $file[count($file)-1];
                    $musicFile         = implode(".",$file);
                    echo "Музыкальный файл извлечен из имени файла cue\n";
                    echo "Муз. файл: \"{$musicFile}\"";
                    break 2;
                }
            }
        }//end for of musicExt
    }//end if ext==cue

    for($j = 0;$j < count($musicExt);$j++){
        if($extention == $musicExt[$j]){
            $musicFile         = $argv[$i];
            $musicFileExt     = $extention;
            if(!empty($cue)){ break 2; }else{ break; }
        }
    }
    //for debug
    unset($extention);
}

if(empty($musicFile)){
    die("Не указан музыкальный файл!\n");
}

//finding cue
if(empty($cue)){
    echo "Не указан cue!\n";
    echo("Поиск cue файла..\n");
        
    if(file_exists ( $musicFile.".cue" )){
        $cue = $musicFile.".cue";
        echo "Файл cue найден первым способом!\n";
    }else{
        
        $file = explode('.', $musicFile);//split for finding extention
        unset($file[count($file)-1]);
        $file = (implode(".",$file)).".cue";
        
        if(file_exists ( $file )){
            $cue = $file;
            echo "Файл cue найден вторым способом!\n";
        }else{
            die("Не был найден cue файл!\n");
        }
    }//end finding cue
    echo "cue: \"{$cue}\"\n";
}

$charcode = trim(exec("uchardet \"{$cue}\""));

$names = parse(file_get_contents($cue), $charcode);


if(!count($names)) {
    die("Ошибка при парсинге cue!\n");
}


for($i = 0; $i < count($names);$i++){
    $timestamps[$i] = $names[$i]['index'];
}

if(count($timestamps) < 2) {
    die("ERROR! Недостаточно timestamp'ов! Проверьте cue\n");
}



echo "Файл cue успешно распарсен!\n";




//парсер mediainfo
//необходим для определения длительности файла
$countKeys = array();
$mediainfo = `mediainfo -f "$musicFile"`;

//$mediainfo = str_replace("\n\n", "\n", $mediainfo);
$mediainfo = explode("\n", $mediainfo);

foreach ($mediainfo as $value) {
    $tmparr =  explode(":", $value);
    $tmparr[0] = trim($tmparr[0]);

    //проверка на уникальность параметра
    if(isset($medinfoPrsd[$tmparr[0]]) && $tmparr[0] != "") {
        $tmp = str_replace(" ", "_", $tmparr[0]);

        if(!isset($countKeys[$tmp])) {
            $countKeys[$tmp] = 0;
        }

        $tmparr[0] = trim($tmparr[0].++$countKeys[$tmp]);
    }

    $medinfoPrsd[$tmparr[0]] = "";

    for($i = 1; $i < count($tmparr); $i++) {
        $medinfoPrsd[$tmparr[0]] = trim($medinfoPrsd[$tmparr[0]].$tmparr[$i]);
    }
}

//cleanup
unset($medinfoPrsd["General"]);
unset($medinfoPrsd["Audio"]);
unset($medinfoPrsd[""]);
$medinfoPrsd = array_unique($medinfoPrsd);//чистка от дубликатов//а зачем?


//make timestamps usable for ffmpeg
for($i = 0; $i < count($timestamps); $i++) {
    $timestamps[$i] = str_replace(".", ":", $timestamps[$i]);
    $timestamps[$i] = explode(":", $timestamps[$i]);

    for($j = 0; $j < count($timestamps[$i]); $j++) {
        $timestamps[$i][$j] = (int)$timestamps[$i][$j];
    }
    
    $timestamps[$i] = ( $timestamps[$i][0] * 60 * 1000 + $timestamps[$i][1] * 1000 + $timestamps[$i][2]);
}



$timestamps[] = trim($medinfoPrsd["Duration"]);//last timestamp

$timeOut = time();

//следящий тред
switch ($pid = pcntl_fork()) {
case -1:
    // @fail
    die('Fork failed');
    break;

case 0:
    while(true) {
        $a = trim(`ps ax | grep $uniqId | grep -v grep | wc -l`);
        echo "\rWorking {$a} threads   ";
        usleep(100);

        if($a == 0 && time() > $timeOut + 5) {
            echo "\r\nDONE!\n";
            exit;
        }
    }

    exit;
    break;

default:
    //parent
    break;
}



chdir($dir);



$countThreads = 0;
$counterThreads = 1;

//wtf?
for($i = 1; $i < count($timestamps); $i++) {
    
    $info = $names[$i - 1];

    $artist = $info['Artist'];
    $album     = $info['Album'];
    $song     = $info['title'];
    $year     = $info['Year'];
    $track     = $info['track#'];
    $total     = $info['numOfTracks'];
    $genre     = $info['genre'];
    $comment = $info['Comment'];
    $descr = "cutted by fish";
    
    $timeStart = $timestamps[$i - 1];
    $timeEnd = $timestamps[$i];

    //TODO: разобраться зачем конвертировать туда сюда. только из за Duration в медиаинфо?
    $hoursStart =    floor( $timeStart / 1000 / 60 / 60);
    $minStart    =    floor(($timeStart - ($hoursStart*1000*60*60)) / 1000 / 60);
    $secStart    =    floor(($timeStart - ($hoursStart*1000*60*60)  - ($minStart*1000*60)) / 1000 );
    $msecStart    =    floor(($timeStart - ($hoursStart*1000*60*60)  - ($minStart*1000*60)  - ($secStart*1000)));
    
    
    $timeEnd = $timeEnd - $timeStart;
    $hoursEnd     =    floor( $timeEnd / 1000 / 60 / 60);
    $minEnd        =    floor(($timeEnd - ($hoursEnd*1000*60*60)) / 1000 / 60);
    $secEnd        =    floor(($timeEnd - ($hoursEnd*1000*60*60)  - ($minEnd*1000*60)) / 1000 );
    $msecEnd    =    floor(($timeEnd - ($hoursEnd*1000*60*60)  - ($minEnd*1000*60)  - ($secEnd*1000)));

    $start = "$hoursStart:$minStart:$secStart.$msecStart";
    $duration = "$hoursEnd:$minEnd:$secEnd.$msecEnd";
        
    if (! function_exists('pcntl_fork')) { die('PCNTL functions not available on this PHP installation'); }


    $pic = 'sh -c \'find -iname "*.jpg" ; find -iname "*.png"\'|head -1';
    
    $pic = trim(exec($pic));
    $picmd = "";
    if(!empty($pic)){
        if(!file_exists("cut_cover.jpg")){
            exec("convert \"$pic\" -resize 240x240 -quality 95 cut_cover.jpg");
        }
        $picmd = " -p cut_cover.jpg ";
    }
    
    if($total < 10)
        $zeros = 1;
    elseif($total < 100)
        $zeros = 2;
    elseif($total < 1000)
        $zeros = 3;
    else
        $zeros = 0;

    switch ($pid = pcntl_fork()) {
    case -1:
        // @fail
        die('Fork failed');
        break;

    case 0:
        // @child: Include() misbehaving code here
        echo "\n------------------------------------------\n";
        echo "thread #{$counterThreads} started!\n";
    
        /*print_r($name);
        exit;
        */
        $track_formatted = str_pad($track, $zeros, '0', STR_PAD_LEFT);
        $fileName = "{$track_formatted} - {$artist} - {$song} ({$year}).{$musicFileExt}";
        
    
        echo "filename: \t{$fileName}\n";

        echo  "Start:\t\t{$start}\n";
        echo  "Duration:\t{$duration}\n";
        
        $command = "echo \"fork {$uniqId} started\" & ffmpeg -y -loglevel fatal -ss {$start} -t {$duration} ".
            "-i \"{$musicFile}\"  -map_metadata -1  \"{$fileName}\" > /dev/null 2>&1";
        

        exec($command);
        
     
        $id3Cmd = "mid3v2 -s --artist=\"{$artist}\" --album=\"{$album}\" --song=\"{$song}\"  --year=\"{$year}\"  ".
            "-T \"{$track}/{$total}\" --genre=\"{$genre}\" -c \"{$descr}\":\"{$comment}\" {$picmd} \"{$fileName}\"";
            
        //echo $id3Cmd;

        if(file_exists($fileName)){
            echo "Cutted  {$fileName} done!\n";
            echo "Using id3 tags\n";
            exec($id3Cmd);
        }
        exit;
        break;//okay...

    default:
        // @parent
        $countThreads ++;
        $counterThreads ++;
        usleep(1500);

        if($countThreads >= $threads) {
            pcntl_waitpid($pid, $status);
            $countThreads -= round(($threads / 2), 1);
        }

        break;
    }
}//end for


while (pcntl_waitpid(0, $status) != -1) {
    $status = pcntl_wexitstatus($status);
}



//удаление исходного файла
echo "ПОЖАЛУЙСТА ПРОВЕРЬТЕ ФАЙЛЫ СОЗДАННЫЕ ПРОГРАММОЙ\n";

$line = "";

while($line == "") {

    echo "\r\nВы хотите удалить исходный flac файл Y/n?";
    $line = trim(fgets(STDIN));
    $line = strtolower($line);

    if(strlen($line) > 1) {
        $line = "";
        echo "\rВведите либо \"y\" либо \"n\". Регистр не имеет значение\r\n";
        continue;
    }

    if($line == "y") {
        unlink($musicFile);
        echo "\rВы удалили исходный файл\r\n";
    }

    elseif($line == "n") {
        echo "\rВы оставили исходный файл\r\n";
    }
    else {
        $line = "";
        continue;
    }
}



//modified cueparser, original - https://github.com/frifox/cueParser
function parse($cue=null, $charcode = "UTF-8") {
    if(!$cue) return false;

    $cue = str_replace("REM ",'REM',$cue);
    // UTF-ize if necessary
    $cue = preg_replace("/^\xef\xbb\xbf/", '', $cue);
    
    if($charcode != 'UTF-8')
        $cue = mb_convert_encoding($cue, 'UTF-8', $charcode);
    
    /*if(!$encoding = mb_detect_encoding($cue)) {
        $cue = mb_convert_encoding($cue, 'UTF-8', 'ISO-8859-1');
    } else {
        $cue = mb_convert_encoding($cue, 'UTF-8', $encoding);
    }*/

    // convert to array
    $cue = str_replace("\r",'',$cue);
    $cue = explode("\n",$cue);

    // dump bad lines
    $allowed = array( 'file', 'track', 'performer', 'title', 'REMDATE', 'REMCOMMENT', 'REMDISCID', 'REMGENRE', 'index' );
    foreach($cue as $key => $value) {
        $line = preg_replace('/[^a-z]/i','',$value);
        $valid = false;
        
        foreach($allowed as $x) {
            if(preg_match("/^$x/i",$line)) $valid = $x;
        }

        if($valid) {
            unset($cue[$key]);
            $cue[$key]['type'] = $valid;
            $cue[$key]['line'] = $value;
        } else {
            unset($cue[$key]);
        }
        unset($line,$valid);
    }

    //remove line type identifier
    foreach($cue as $key => $value) {
        $line = $value['line'];
        $chars = preg_split('//', $value['type'], -1, PREG_SPLIT_NO_EMPTY);
        foreach($chars as $char) {
            $line = preg_replace("/^.*?$char/i",'',$line);
        }
        $cue[$key]['line'] = $line;
        unset($line,$chars);
    }

    // clean up lines & convert index->frames
    $track = "";
    foreach($cue as $key => $value) {

        //print_r($value);
        $line = $value['line'];
        switch($value['type']) {
        case 'file':
            $line = preg_replace('/^[ |"|\t]*(.*?)[ |"|\t]*$/','$1',$line); // wrappers
            $line = preg_replace('/\w+$/','',$line); // filetype identifier
            $line = preg_replace('/^[ |"|\t]*(.*?)[ |"|\t]*$/','$1',$line); // wrappers
            $line = preg_replace('/\t+/',' ',$line); // tabs
            $line = preg_replace('/ +/',' ',$line); // multi-spaces
            $cue[$key]['line'] = $line;
            break;
        case 'track':
            $cue[$key]['track'] = $value['line'];
            $track = $value['line'];
            break;
        case 'index':
            $line = preg_replace('/^[ |"|\t]*(.*?)[ |"|\t]*$/','$1',$line); // wrappers
            $line = preg_replace('/^\w+/','',$line); // leading identifier
            $line = preg_replace('/[^\d|:]+/','',$line); // everyting non-digit or :
            $cue[$key]['line'] = $line;

            // convert to frames
            break;
        default:
            $line = preg_replace('/^[ |"|\t]*(.*?)[ |"|\t]*$/','$1',$line); // wrappers
            $line = preg_replace('/\t+/',' ',$line); // tabs
            $line = preg_replace('/ +/',' ',$line); // multi-spaces

            
            $cue[$key]['track'] = (int)$track;
            $cue[$key]['line'] = $line;
            break;
        }
        //print_r($line);
        //print("\n");
        unset($line,$frames);
    }



    // retrieve CUESHEET data
    $allowed = array('performer', 'TRACK', 'REMDISCID', 'REMCOMMENT', 'REMGENRE', 'title', 'REMDATE', 'file', 'TITLE' );
               
    foreach($cue as $key => $value) {
        if($value['type']=='track') {
            echo $value['type']."\n";
            break; // reached tracks, stop
        }

        if(in_array($value['type'],$allowed)) {
            $out['Cuesheet'][$value['type']] = $value['line'];
        }
        unset($cue[$key]);
    }
    
    //to cueprint style
    $albumInfo['Album'] = $out['Cuesheet']['title'];
    $albumInfo['Year'] = $out['Cuesheet']['REMDATE'];
    $albumInfo['DiscID'] = $out['Cuesheet']['REMDISCID'];
    $albumInfo['Comment'] = $out['Cuesheet']['REMCOMMENT'];
    $albumInfo['genre'] = $out['Cuesheet']['REMGENRE'];
    $albumInfo['Artist'] = $out['Cuesheet']['performer'];
    $albumInfo['arranger'] = $out['Cuesheet']['arranger'];
    $albumInfo['composer'] = $out['Cuesheet']['composer'];
    
    //print_r($albumInfo);
    

    // retrieve track data
    $allowed = array(
                   'performer',
                   'title',
                   'TRACK',
                   'REMDISCID',
                   'REMCOMMENT',
                   'REMGENRE',
                   'DATE',
                   'TITLE',
                   'index'
               );

    $max = 0;
    $count = 0;
    
    foreach($cue as $key => $value) {
        
        if($value['type']=='track') {
            @$i++;             
            $out['Track'][$i] = $albumInfo;//init album info
            $out['Track'][$i]['track#'] = (int)$value['track'];
        } 
        if(isset($value['track']) && $value['track'] > $max){
            $max = $value['track'];
        }
        if(in_array(trim($value['type']), $allowed)) {
            $out['Track'][$i][trim($value['type'])] = trim($value['line']);
            unset($cue[$key]);
        }
    }
    
    $out = array_values($out['Track']);
    
    for($j = 0; $j < count($out);$j++){
        $out[$j]['numOfTracks'] = $i;
    }
    
    // done, return it
    
    return $out;
}
